#!/usr/bin/bash
#

echo ; read -p 	"which java edition (i.e. '8-jdk', '17-jre') do you want to install? " \
	java_edition ; echo

# ##############################################################################
wget_installed()	{
		wget --version >/dev/null 2>&1
	}

# ######################################
install_wget()		{
		apt update && apt upgrade -y
		apt install wget -y
	}

# ######################################
java_installed()	{
		java -version >/dev/null 2>&1
	}

# ######################################
install_java()	{
		# installing jdk, if it doesn't exist
		add_adoptium_repo
		apt update && apt upgrade -y
		apt install temurin-"${java_edition}" -y
		echo "$(java -version | tail -1) has been installed"
	}

# ######################################
add_adoptium_repo()	{
		# installing wget, if it does not exist
		# echo wget-status in add_adoptiom_repo: "${wget_status}" ; sleep 11

		if ! wget_installed ; then
			echo ; echo installing wget now; echo
			install_wget
		else
			echo ; echo wget already exists. adding the adoptium repo now ; echo
		fi
		
		# add adoptium repo, if it does not exist
		if [[ ! -s /etc/apt/sources.list.d/adoptium.list ]] ; then
			wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public \
				| tee /etc/apt/keyrings/adoptium.asc
			echo "deb [signed-by=/etc/apt/keyrings/adoptium.asc] \
				https://packages.adoptium.net/artifactory/deb \
				$(awk -F= '/^VERSION_CODENAME/{print$2}' \
				/etc/os-release) main" \
				| tee /etc/apt/sources.list.d/adoptium.list
		else
			echo the Adoptium repo already exists. Installing java \
				"${java_edition}" now ; echo
		fi
	}

# ### doing the work ###########################################################

java_installed
echo java status: "${?}" ; sleep 1

wget_installed
echo wget status: "${?}" ; echo ; sleep 1


# install java, if you have to #################################################

if java_installed ; then
	echo java $(java -version) is already installed
	exit 0
else
	add_adoptium_repo
	install_java
fi

exit 0

